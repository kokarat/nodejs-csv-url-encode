/**
* CSV affiliate endcode affiliate link to base64
*/
const args = process.argv;
var fileName = args[2];
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var ProgressBar = require('progress');
var dataArray = [];

fs.createReadStream(fileName)
.pipe(csv())
.on('data', function (data) {
    data.base64 = new Buffer(data.AffiateLink).toString('base64');
    dataArray.push(data);
})
.on('end', function(){
    try {
        var bar = new ProgressBar(':bar', { total: 100});
        var result = json2csv({ data: dataArray, fields: Object.keys(dataArray[0]) });    
            bar.tick(result.length);
        var writeFile = fs.writeFileSync(fileName+'_new.csv', result,'utf8');

        if(bar.complete){
            console.log('=> Process file '+ fileName + ' is Done!. File is saved to file name =>'+ fileName+'_new.csv');
            //console.log('File is saved to file name => '+ fileName+'_new.csv');
        }
    } catch (error) {
        console.log(error);
    }
    
});